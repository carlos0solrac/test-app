import { async, inject, TestBed } from '@angular/core/testing';
import { AlertController, Platform } from '@ionic/angular';


import { AlertService } from './alert.service';

let platformReadySpy;
let platformSpy;
let alertService: AlertService;

describe('AlertService', () => {

  beforeEach(async(() => {
    platformReadySpy = Promise.resolve();
    platformSpy = jasmine.createSpyObj('Platform', { ready: platformReadySpy });

    TestBed.configureTestingModule({
      providers: [
        { provide: Platform, useValue: platformSpy },
        { provide: AlertController },
        { provide: AlertService }

      ]
    }).compileComponents();
  }));

  beforeEach(inject([AlertService], (service: AlertService) => {
    alertService = service;
  }));

  it('should exist', () => {
    expect(alertService).toBeDefined();
  });
});

import { NgModule } from '@angular/core';
import { AlertService } from './alert.service';


@NgModule({
  declarations: [],
  imports: [],
  providers: [AlertService]
})
export class AlertServiceModule { }

import { Injector } from '@angular/core';
import { async, inject, TestBed } from '@angular/core/testing';
import { ApiService } from './api.service';
import { InjectorService } from '../injector';
import { Platform } from '@ionic/angular';



let apiService: ApiService;
let platformReadySpy;
let platformSpy;

describe('ApiService', () => {

  beforeEach(async(() => {
    platformReadySpy = Promise.resolve();
    platformSpy = jasmine.createSpyObj('Platform', { ready: platformReadySpy });

    TestBed.configureTestingModule({
      providers: [
        { provide: Platform, useValue: platformSpy },
        { provide: ApiService, useClass: ApiService }
      ]
    }).compileComponents();
  }));
  beforeEach(inject([Injector], (injectorParam: Injector) => {
    InjectorService.injector = injectorParam;
  }));

  beforeEach(inject([ApiService], (apiService2: ApiService) => {
    apiService = apiService2;
  }));

  it('should exist', () => {
    expect(apiService).toBeDefined();
  });
});

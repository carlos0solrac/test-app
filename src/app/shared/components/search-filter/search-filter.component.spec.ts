import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SearchFilterComponent } from './search-filter.component';

describe('SearchFilterComponent', () => {
  let app;
  let component: SearchFilterComponent;
  let fixture: ComponentFixture<SearchFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchFilterComponent],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SearchFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchFilterComponent);
    app = fixture.debugElement.componentInstance;
  });

  it('should create the search filter component', () => {
    expect(app).toBeTruthy();
  });

  afterEach(() => {
    fixture.destroy();
    app = null;
  });


  it('Funcion: getItems', () => {
    const var2 = 'text';
    const array = [{ id: 0, photo: 'https://picsum.photos/id/0/500/500', text: 'Afvevhjjjjhkjhkj', load: false }];
    const var1 = 'id';
    const ev = 'Af';
    app.getItems(ev, array, [var1, var2]);
    expect([{ id: 0, photo: 'https://picsum.photos/id/0/500/500', text: 'Afvevhjjjjhkjhkj', load: false }]).toEqual([{ id: 0, photo: 'https://picsum.photos/id/0/500/500', text: 'Afvevhjjjjhkjhkj', load: false }]);

  });


  it('Funcion: getItems', () => {
    const var2 = 'text';
    const array = [{ id: 0, photo: 'https://picsum.photos/id/0/500/500', text: 'Afvevhjjjjhkjhkj', load: false }];
    const var1 = 'id';
    const ev = 'Ax';
    app.getItems(ev, array, [var1, var2]);
    expect([]).toEqual([]);
  });
});

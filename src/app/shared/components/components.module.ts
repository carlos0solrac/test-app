import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { PipesModule } from '../pipes/pipes.module';
import { SearchFilterComponent } from './search-filter/search-filter.component';
import { ItemPhotoComponent } from './item-photo/item-photo.component';

const components: any = [
  SearchFilterComponent,
  ItemPhotoComponent
];

@NgModule({
  declarations: components,
  exports: components,
  imports: [CommonModule, IonicModule, PipesModule]
})

export class ComponentsModule {
}
